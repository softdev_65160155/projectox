/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.softdev;

/**
 *
 * @author Lenovo
 */
import java.util.Scanner;
public class ProjectOX {
    private char[][] gboard;
    private char player;
    private boolean endgame;
    private char Continue;
    private boolean playcon; 
    private int pround = 1;

  public ProjectOX(){
    gboard= new char[3][3];
    player = 'X';
    endgame = false;

    for(int row = 0; row <3; row++){
        for(int col = 0; col<3; col++){
            gboard[row][col] ='-';
        }
    }
  }

  //Play Game Here
  public void playOX(){
    
    System.out.println("Welcome to OX");
    playcon = true;
    while(playcon){
        resetboard();
        resetround();
        endgame =false; 
        while(!endgame){
            printBoard();
            System.out.println("Round "+pround);
            playermoved();
            gamestatus();
            switchplayer();
    }
    System.out.println("Result");
    System.out.println("*----*");
    printBoard();
     System.out.println("*----*");

    Scanner kb2 = new Scanner(System.in);
    System.out.println("Continue? (y/n):");
    Continue = kb2.next().charAt(0);
    if(Continue=='y'){
        playcon =true;
    }
    else{
        playcon = false;
    }
} 
  }
  //method to Resetround
  public void resetround(){
    pround=1;
  }
  //method to Resetboard when play continue
  public void  resetboard(){
    for(int row = 0; row <3; row++){
        for(int col = 0; col<3; col++){
            gboard[row][col] ='-';
        }
    }
  }

  public void printBoard(){
    for(int row = 0; row <3;row++){
    
        for(int col =0;col<3;col++){
            System.out.print(gboard[row][col]+" ");

        }
        System.out.println(" ");
    }
  }
    
  
  public void playermoved(){
    Scanner kb = new Scanner(System.in);
    int row,col;
    do{
        System.out.print("Player " + player +" input row[1-3] col[1-3]:");
        row = kb.nextInt()-1;
        col = kb.nextInt()-1; 

    }
    while(!Validmove(row,col));
     gboard[row][col] = player;

  }
  
//Check space that can move
  public boolean Validmove(int row,int col){
    if (row < 0 || row >= 3 || col < 0 || col >= 3 || gboard[row][col] !='-'){
        System.out.println("Invalid move, Please Move again!");
        return false;
    }
    return true;
  }

  //Check who winner or tie
  public void gamestatus(){

    if(checkwinner()){
        endgame = true;
        System.out.println("Player "+ player+" Wins!!");
       

    }
    if(boardfull()){
        endgame = true;
        System.out.println("Tie!!");
      
      
    }

  }
  
  public boolean checkwinner(){
    //Check row 
    // Ex.
    // x x x or o o o
    for(int row =0;row<3;row++){
      if(gboard[row][0] == player && gboard[row][1] == player && gboard[row][2] == player){
         return true;
      }
    }
    //Check col 
    // Ex.
    // x or o
    // x    o
    // x    o
    for(int col =0;col<3;col++){
      if(gboard[0][col] == player && gboard[1][col] == player && gboard[2][col] == player){
         return true;
      }
    }
    //Check diagonals
    //x   x   or   o   o
    //  X            o
    //x   x        o   o
    if(gboard[0][0] == player && gboard[1][1] == player && gboard[2][2] == player){
        return true;
    }
    if(gboard[2][0] == player && gboard[1][1] == player && gboard[0][2] == player){
        return true;
    }

    return false;
  }
  
  public boolean boardfull(){
    for(int row = 0; row <3;row++){
        for(int col =0; col<3;col++){
          if(gboard[row][col]== '-'){
            return false;
          }
        }
  } return true;
  }
  public void switchplayer(){
    if(player =='X'){
        player='O';
        pround++;

    }
    else{
        player='X';
        pround++;
      
    }
  }

    public static void main(String[] args) {
       ProjectOX OX = new ProjectOX();
       OX.playOX();
    }
}
